import { Hash } from "./hash.algo";
import { EncryptionGroup } from "../model/encryption-group";
import { IdentificationGroup } from "../model/identification-group";
import { PrimesCache } from "../model/primes-cache";
import { Conversion } from "./conversion.algo";
import { Preconditions } from "../tools/preconditions";
import { BigInteger } from "../model/big-integer";

/**
 * This class regroups the general algorithms described in Section 7.2 of the specification
 */
export class GeneralAlgorithms {

  constructor(private hashService: Hash,
              private encryptionGroup: EncryptionGroup,
              private identificationGroup: IdentificationGroup,
              private primesCache: PrimesCache) {
    Preconditions.checkArgument(encryptionGroup.equals(primesCache.encryptionGroup));
  }

  /**
   * Algorithm 7.1: GetPrimes
   * <p>This implementation makes use of a cache, as suggested in the comment of the algorithm</p>
   *
   * @param n the number of requested primes (>= 0)
   * @return the ordered list of the n first primes found in the group
   * @throws Error if there are not enough primes in the cache.
   */
  public getPrimes(n: number): BigInteger[] {
    Preconditions.checkArgument(n >= 0, 'n must be greater or equal to 0');
    let primes: BigInteger[] = this.primesCache.primes;
    if (primes.length < n) {
      throw new Error(`${n} primes requested, ${primes.length} available`);
    }
    return primes.slice(0, n);
  }

  /**
   * Algorithm 7.2 : isMember
   *
   * @param x a number
   * @return true if x is in encryptionGroup, false otherwise
   */
  public isMember(x: BigInteger): boolean {
    return x.cmp(BigInteger.ONE) >= 0
      && x.cmp(this.encryptionGroup.p) < 0
      && x.legendre(this.encryptionGroup.p) === 1;
  }

  /**
   * Utility to verify membership for G_q_hat
   *
   * @param x a number
   * @return true if x is in identificationGroup, false otherwise
   */
  public isMember_G_q_hat(x: BigInteger): boolean {
    return x.cmp(BigInteger.ONE) >= 0
      && x.cmp(this.identificationGroup.p_hat) < 0
      && x.legendre(this.identificationGroup.p_hat) === 1;
  }

  /**
   * Utility to verify membership for Z_q
   *
   * @param x a number
   * @return true if x &isin; Z_q, false otherwise
   */
  public isInZ_q(x: BigInteger): boolean {
    return x.cmp(BigInteger.ZERO) >= 0
      && x.cmp(this.encryptionGroup.q) < 0;
  }

  /**
   * Utility to verify membership for Z_q_hat
   *
   * @param x a number
   * @return true if x &isin; Z_q_hat, false otherwise
   */
  public isInZ_q_hat(x: BigInteger): boolean {
    return x.cmp(BigInteger.ZERO) >= 0
      && x.cmp(this.identificationGroup.q_hat) < 0;
  }

  /**
   * Algorithm 7.3: GetGenerators
   * Create a number of independent generators for the encryption group given
   *
   * @param n number of generators to be computed
   * @return a list of independent generators
   */
  public getGenerators(n: number): BigInteger[] {
    let h: BigInteger[] = [];
    let valuesToAvoid: BigInteger[] = [];
    valuesToAvoid.push(BigInteger.ZERO);
    valuesToAvoid.push(BigInteger.ONE);

    for (let i: number = 0; i < n; i++) {
      let h_i: BigInteger;
      let x: number = 0;
      do {
        x++;
        // "Magic" constants "chVote" and "ggen" as given in the protocol specification
        let bytes: Uint8Array = this.hashService.recHash_L(['chVote', 'ggen', new BigInteger(i), new BigInteger(x)]);
        h_i = Conversion.toInteger(bytes).mod(this.encryptionGroup.p);
        h_i = h_i.square().mod(this.encryptionGroup.p);
      } while (GeneralAlgorithms.contains(valuesToAvoid, h_i)); // Very unlikely, but needs to be avoided
      h.push(h_i);
      valuesToAvoid.push(h_i);
    }
    return h;
  }

  private static contains(set: BigInteger[], value: BigInteger): boolean {
    for (let i: number = 0; i < set.length; i++) {
      if (set[i].equals(value)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Algorithm 7.4: GetNIZKPChallenge
   *
   * @param y     the public values vector (domain unspecified)
   * @param t     the commitments vector (domain unspecified)
   * @param kappa the soundness strength of the challenge
   * @return the computed challenge
   */
  public getNIZKPChallenge(y: any[], t: any[], kappa: number): BigInteger {
    return Conversion.toInteger(this.hashService.recHash_L([y, t])).mod(new BigInteger(Math.pow(2, kappa)));
  }

  /**
   * Algorithm 7.5: GetNIZKPChallenges
   *
   * @param n     the number of challenges requested
   * @param y     the public values vector (domain unspecified)
   * @param kappa the soundness strength of the challenge
   * @return a list challenges, of length n
   */
  public getNIZKPChallenges(n: number, y: any[], kappa: number): BigInteger[] {
    let upper_h: Uint8Array = this.hashService.recHash_L(y);
    let two_to_kappa: BigInteger = new BigInteger(Math.pow(2, kappa));

    // TODO: run in parallel!
    let challenges: BigInteger[] = [];
    for (let i: number = 1; i <= n; i++) {
      let upper_i: Uint8Array = this.hashService.hash_L(new BigInteger(i));
      challenges.push(
        Conversion.toInteger(
          this.hashService.hash_L(
            GeneralAlgorithms.concatenate(upper_h, upper_i)
          )
        ).mod(two_to_kappa)
      );
    }

    return challenges;
  }

  /**
   * Concatenates the given Unit8Arrays
   *
   * @param arrays the arrays to concatenate
   * @returns the concatenation result
   */
  public static concatenate(...arrays: Uint8Array[]): Uint8Array {
    let totalLength = 0;
    for (let arr of arrays) {
      totalLength += arr.length;
    }
    let result = new Uint8Array(totalLength);
    let offset = 0;
    for (let arr of arrays) {
      result.set(arr, offset);
      offset += arr.length;
    }
    return result;
  }

  /**
   * Truncate function, as defined in section 4.1 <strong>Byte Arrays</strong>
   *
   * @param a the array to be truncate
   * @param length the requested length
   * @return a copy of the array, truncated to the requested length
   */
  public static truncate(a: Uint8Array, length: number): Uint8Array {
    Preconditions.checkArgument(a.length >= length, 'The given array is smaller than the requested length');
    return Uint8Array.from(a.slice(0, length));
  }

  /**
   * Performs a XOR operation between two arrays
   *
   * @param a first array
   * @param b second array
   * @returns the XOR result
   */
  public static xor(a: Uint8Array, b: Uint8Array): Uint8Array {
    Preconditions.checkArgument(a.length == b.length, `The arrays should have the same size. |a| = [${a.length}], |b| = [${b.length}]`);
    let local_a: Uint8Array = Uint8Array.from(a);
    let local_b: Uint8Array = Uint8Array.from(b);
    let result: Uint8Array = new Uint8Array(local_a.length);
    for (let i: number = 0; i < local_a.length; i++) {
      result[i] = local_a[i] ^ local_b[i];
    }
    return result;
  }

  /**
   * Extract function, as defined in section 4.1 <strong>Byte Arrays</strong>
   *
   * @param a the array from which to extract the values
   * @param start the starting position (inclusive)
   * @param end the ending position (exclusive)
   * @return a copy of the range of the array between start (incl.) and end (excl.)
   */
  public static extract(a: Uint8Array, start: number, end: number): Uint8Array {
    Preconditions.checkArgument(start >= 0, 'Start index must be non-negative');
    Preconditions.checkArgument(start < end, 'The starting position must be strictly smaller than the ending position');
    Preconditions.checkArgument(a.length >= end, `The ending position may not be larger than the array's length`);
    let local_a: Uint8Array = Uint8Array.from(a);
    return local_a.slice(start, end);
  }

  /**
   * Algorithm 4.1: MarkByteArray
   * <p>
   * Adds an integer watermark <tt>m</tt> to the bits of a given byte array.
   * The bits of the watermark are spread equally across the bits of the byte array.
   * </p>
   *
   * @param upper_b the byte array to watermark
   * @param m the watermark: 0 <= m <= m_max
   * @param m_max the maximal watermark: ||m_max|| <= 8 * |upper_b|
   * @return the watermarked byte array
   */
  public static markByteArray(upper_b: Uint8Array, m: number, m_max: number): Uint8Array {
    Preconditions.checkArgument(0 <= m, 'm must be non-negative');
    Preconditions.checkArgument(m <= m_max, 'm must be smaller or equal to m_max');
    Preconditions.checkArgument(GeneralAlgorithms.bitLength(m_max) <= 8 * upper_b.length, 'm_max must be smaller or equal to the number of bits in upper_b');

    let l: number = GeneralAlgorithms.bitLength(m_max);
    let s: number = (8 * upper_b.length) / l;
    let local_upper_b: Uint8Array = Uint8Array.from(upper_b);
    let local_m: number = m;

    for (let i: number = 0; i <= l - 1; i++) {
      local_upper_b = GeneralAlgorithms.setBit(local_upper_b, Math.floor(i * s), local_m % 2 == 1);
      local_m = Math.floor(local_m / 2);
    }

    return local_upper_b;
  }


  /**
   * Algorithm 4.2: SetBit
   * <p>
   * Sets the i-th bit of a byte array B to b in (0,1)
   * </p>
   *
   * @param upper_b the byte array
   * @param i the position of the bit that must be set
   * @param b the value which the bit will take
   * @return the modified byte array
   */
  private static setBit(upper_b: Uint8Array, i: number, b: boolean): Uint8Array {
    Preconditions.checkArgument(0 <= i, 'i must be non-negative');
    Preconditions.checkArgument(i <= 8 * upper_b.length, 'i must be smaller or equal to the number of bits in upper_b');

    let local_upper_b: Uint8Array = Uint8Array.from(upper_b);
    let j: number = Math.floor(i / 8);
    let x: number = Math.pow(2, i % 8);

    if (!b) {
      local_upper_b[j] = local_upper_b[j] & (0xFF - x);
    } else {
      local_upper_b[j] = local_upper_b[j] | x;
    }

    return local_upper_b;
  }

  private static bitLength(value: number): number {
    if (value == 0)
      return 0;
    let n: number = 1;
    if (value >>> 16 == 0) {
      n += 16;
      value <<= 16;
    }
    if (value >>> 24 == 0) {
      n += 8;
      value <<= 8;
    }
    if (value >>> 28 == 0) {
      n += 4;
      value <<= 4;
    }
    if (value >>> 30 == 0) {
      n += 2;
      value <<= 2;
    }
    n -= value >>> 31;
    return 32 - n;
  }

}
