/**
 * Utility class used for preconditions checks.
 */
export class Preconditions {

  /**
   * Check a condition and throw an Error if not met
   *
   * @param expression the expression to validate
   * @param errorMessage the error message to send if expression is not met
   */
  public static checkArgument(expression: boolean, errorMessage?: string): void {
    if (!expression) {
      throw new Error(errorMessage);
    }
  }

  /**
   * Check a given object is not null
   *
   * @param object the object to verify
   */
  public static checkNotNull(object: any): void {
    if (object === null || object === undefined) {
      throw new Error('null object');
    }
  }
}
