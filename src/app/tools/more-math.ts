export class MoreMath {

  public static log2(value: number): number {
    return Math.log(value) / Math.log(2.0);
  }
}
