import { NonInteractiveZkpJson } from "./non-interactive-zkp-json";

export class ConfirmationJSON {

  constructor(public readonly y_hat: string,
              public readonly pi: NonInteractiveZkpJson) {
  }
}
