import { BigIntPair } from './big-int-pair';
import { NonInteractiveZkp } from './non-interactive-zkp';
import { BigInteger } from './big-integer';
import { BallotAndQueryJson } from './ballot-and-query-json';

export class BallotAndQuery {

  constructor(public readonly x_hat: BigInteger,
              public readonly bold_a: BigIntPair[],
              public readonly pi: NonInteractiveZkp) {
  }

  toJSON(): BallotAndQueryJson {
    return new BallotAndQueryJson(
      this.x_hat.toBase64String(),
        this.bold_a.map(value => value.toJSON()),
        this.pi.toJSON()
      );
  }
}
