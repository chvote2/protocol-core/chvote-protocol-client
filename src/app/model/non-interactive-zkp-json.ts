export class NonInteractiveZkpJson {

  constructor(public readonly t: String[],
              public readonly s: String[]) {
  }

}
