export class EncryptionGroupJSON {

  constructor(public readonly p: string,
              public readonly q: string,
              public readonly g: string,
              public readonly h: string) {
  }
}
