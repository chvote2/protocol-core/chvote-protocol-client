import { EncryptionGroup } from "./encryption-group";
import { BigInteger } from "./big-integer";

export class EncryptionPrivateKey {

  constructor(public readonly privateKey: BigInteger,
              public readonly encryptionGroup: EncryptionGroup) {
  }
}
