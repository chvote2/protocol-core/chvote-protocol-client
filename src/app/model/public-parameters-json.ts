import { SecurityParameters } from "./security-parameters";
import { EncryptionGroupJSON } from "./encryption-group-json";
import { IdentificationGroupJSON } from "./identification-group-json";

export class PublicParametersJSON {

  constructor(public readonly securityParameters: SecurityParameters,
              public readonly encryptionGroup: EncryptionGroupJSON,
              public readonly identificationGroup: IdentificationGroupJSON,
              public readonly primeField: PrimeField,
              public readonly q_hat_x: string,
              public readonly upper_a_x: string[],
              public readonly q_hat_y: string,
              public readonly upper_a_y: string[],
              public readonly upper_a_r: string[],
              public readonly upper_l_r: number,
              public readonly upper_a_f: string[],
              public readonly upper_l_f: number,
              public readonly s: number,
              public readonly n_max: number) {
  }
}

export class PrimeField {

  constructor(public readonly p_prime: string) {
  }
}
