import { BigIntPairJSON } from './big-int-pair-json';
import { NonInteractiveZkpJson } from './non-interactive-zkp-json';


export class BallotAndQueryJson {

  constructor(public readonly x_hat: String,
              public readonly bold_a: BigIntPairJSON[],
              public readonly pi: NonInteractiveZkpJson) {
  }

}
