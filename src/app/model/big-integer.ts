///<reference path="../../typings.d.ts"/>

export class BigInteger {

  private readonly wrapper: verificatum.arithm.LargeInteger;

  public static ZERO: BigInteger = new BigInteger(verificatum.arithm.LargeInteger.ZERO);
  public static ONE: BigInteger = new BigInteger(verificatum.arithm.LargeInteger.ONE);
  public static TWO: BigInteger = new BigInteger(verificatum.arithm.LargeInteger.TWO);

  constructor(value: string | number | number[] | verificatum.arithm.LargeInteger, randomDevice?: verificatum.crypto.RandomDevice) {
    if (value instanceof verificatum.arithm.LargeInteger) {
      this.wrapper = <verificatum.arithm.LargeInteger>value;

    } else if (typeof value === 'number' && !randomDevice) {
      let n: number = <number>value;
      this.wrapper = new verificatum.arithm.LargeInteger(n.toString(16));

    } else if (typeof value === 'string' && !randomDevice) {
      this.wrapper = new verificatum.arithm.LargeInteger(
        atob(<string> value).split('').map(function (c) {
          return c.charCodeAt(0);
        })
      );

    } else {
      this.wrapper = new verificatum.arithm.LargeInteger(value, randomDevice);
    }
  }

  public toString(): string {
    return this.toBase64String();
  }

  public toBase64String(): string {
    return btoa(String.fromCharCode.apply(null, this.toByteArray()));
  }

  public abs(): BigInteger {
    return new BigInteger(this.wrapper.abs());
  }

  public mod(modulus: BigInteger): BigInteger {
    return new BigInteger(this.wrapper.mod(modulus.wrapper));
  }

  public modPow(exponent: BigInteger, modulus: BigInteger, naive?: boolean): BigInteger {
    return new BigInteger(this.wrapper.modPow(exponent.wrapper, modulus.wrapper, naive));
  }

  public modInv(modulus: BigInteger): BigInteger {
    return new BigInteger(this.wrapper.modInv(modulus.wrapper));
  }

  public toHexString(): string {
    return this.wrapper.toHexString();
  }

  public toByteArray(n?: number): Uint8Array {
    return this.wrapper.toByteArray(n);
  }

  public bitLength(): number {
    return this.wrapper.bitLength();
  }

  public cmp(other: BigInteger): number {
    return this.wrapper.cmp(other.wrapper);
  }

  public neg(): BigInteger {
    return new BigInteger(this.wrapper.neg());
  }

  public add(other: BigInteger): BigInteger {
    return new BigInteger(this.wrapper.add(other.wrapper));
  }

  public sub(other: BigInteger): BigInteger {
    return new BigInteger(this.wrapper.sub(other.wrapper));
  }

  public mul(other: BigInteger): BigInteger {
    return new BigInteger(this.wrapper.mul(other.wrapper));
  }

  public square(): BigInteger {
    return new BigInteger(this.wrapper.square());
  }

  public divQR(other: BigInteger): BigInteger[] {
    let result: verificatum.arithm.LargeInteger[] = this.wrapper.divQR(other.wrapper);
    return [new BigInteger(result[0]), new BigInteger(result[1])];
  }

  public equals(other: BigInteger): boolean {
    return this.wrapper.equals(other.wrapper);
  }

  public legendre(prime: BigInteger): number {
    return this.wrapper.legendre(prime.wrapper);
  }

  public getBit(index: number): number {
    return this.wrapper.getBit(index);
  }

  public iszero(): boolean {
    return this.wrapper.iszero();
  }

  public shiftRight(offset: number): BigInteger {
    return new BigInteger(this.wrapper.shiftRight(offset));
  }

}