import { EncryptionPublicKey } from "./encryption-public-key";
import { EncryptionPrivateKey } from "./encryption-private-key";

export class EncryptionKeyPair {

  constructor(public readonly encryptionPublicKey: EncryptionPublicKey,
              public readonly encryptionPrivateKey: EncryptionPrivateKey) {
  }

}