import { SecurityParameters } from "./security-parameters";
import { EncryptionGroup } from "./encryption-group";
import { IdentificationGroup } from "./identification-group";
import { Preconditions } from "../tools/preconditions";
import { MoreMath } from "../tools/more-math";
import { BigInteger } from "./big-integer";
import { PublicParametersJSON } from "./public-parameters-json";

export class PublicParameters {

  public readonly l_x: number;
  public readonly l_y: number;
  public readonly l_r: number;
  public readonly l_f: number;
  public readonly upper_l_m: number;

  constructor(public readonly securityParameters: SecurityParameters,
              public readonly encryptionGroup: EncryptionGroup,
              public readonly identificationGroup: IdentificationGroup,
              public readonly p_prime: BigInteger,
              public readonly q_hat_x: BigInteger,
              public readonly upper_a_x: Uint8Array,
              public readonly q_hat_y: BigInteger,
              public readonly upper_a_y: Uint8Array,
              public readonly upper_a_r: Uint8Array,
              public readonly upper_l_r: number,
              public readonly upper_a_f: Uint8Array,
              public readonly upper_l_f: number,
              public readonly s: number,
              public readonly n_max: number) {
    Preconditions.checkArgument(encryptionGroup.h.cmp(BigInteger.TWO) >= 0, "h should be greater than or equal to 2");
    Preconditions.checkArgument(2 * securityParameters.tau <= identificationGroup.q_hat.bitLength(), "2 * tau must be smaller than the bit length of q_hat");
    Preconditions.checkArgument(s >= 1, "There must be at least one authority");
    Preconditions.checkArgument(q_hat_x.bitLength() >= 2 * securityParameters.tau, "q_hat_x must be >= 2 * tau");
    Preconditions.checkArgument(q_hat_x.cmp(identificationGroup.q_hat) <= 0, "q_hat_x must be <= q_hat");
    Preconditions.checkArgument(upper_a_x.length >= 2, "|upper_a_x| >= 2");
    Preconditions.checkArgument(q_hat_y.bitLength() >= 2 * securityParameters.tau, "q_hat_y must be >= 2 * tau");
    Preconditions.checkArgument(q_hat_y.cmp(identificationGroup.q_hat) <= 0, "q_hat_y must be <= q_hat");
    Preconditions.checkArgument(upper_a_y.length >= 2, "|upper_a_y| >= 2");
    Preconditions.checkArgument(n_max >= 2, "n_max >= 2");
    Preconditions.checkArgument(8 * upper_l_r >= Math.log((n_max - 1) / (1.0 - securityParameters.epsilon)), "8 * upper_l_r >= log( ( n_max - 1 ) / ( 1 - epsilon) )");
    Preconditions.checkArgument(upper_a_r.length >= 2, "|upper_a_r| >= 2");
    Preconditions.checkArgument(8 * upper_l_f >= Math.log(1.0 / (1.0 - securityParameters.epsilon)), "8 * upper_l_f >= log( 1 / ( 1 - epsilon) )");
    this.l_x = Math.ceil(q_hat_x.bitLength() / MoreMath.log2(upper_a_x.length));
    this.l_y = Math.ceil(q_hat_y.bitLength() / MoreMath.log2(upper_a_y.length));
    this.l_r = Math.ceil(8.0 * upper_l_r / MoreMath.log2(upper_a_r.length));
    this.l_f = Math.ceil(8.0 * upper_l_f / MoreMath.log2(upper_a_f.length));
    this.upper_l_m = 2 * Math.ceil(p_prime.bitLength() / 8.0);
  }

  static fromJSON(json: PublicParametersJSON | string): PublicParameters {
    if (typeof json === 'string') {
      return JSON.parse(json, PublicParameters.reviver);

    } else {

      return new PublicParameters(
        json.securityParameters,
        EncryptionGroup.fromJSON(json.encryptionGroup),
        IdentificationGroup.fromJSON(json.identificationGroup),
        new BigInteger(json.primeField.p_prime),
        new BigInteger(json.q_hat_x),
        this.toArray(json.upper_a_x),
        new BigInteger(json.q_hat_y),
        this.toArray(json.upper_a_y),
        this.toArray(json.upper_a_r),
        json.upper_l_r,
        this.toArray(json.upper_a_f),
        json.upper_l_f,
        json.s,
        json.n_max
      );
    }
  }

  static reviver(key: string, value: any): any {
    return key === "" ? PublicParameters.fromJSON(value) : value;
  }

  static toArray(alphabet: string[]): Uint8Array {
    let result: Uint8Array = new Uint8Array(alphabet.length);

    for (let i: number = 0; i < alphabet.length; i++) {
      result[i] = alphabet[i].charCodeAt(0);
    }

    return result;
  }

}
