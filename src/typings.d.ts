/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare namespace verificatum {
  namespace arithm {
    class LargeInteger {
      public static ZERO: LargeInteger;
      public static ONE: LargeInteger;
      public static TWO: LargeInteger;

      constructor(first: string | number | number[], second?: verificatum.crypto.RandomDevice | number[]);

      public abs(): LargeInteger;

      public mod(modulus: LargeInteger): LargeInteger;

      public modPow(exponent: LargeInteger, modulus: LargeInteger, naive?: boolean): LargeInteger;

      public modInv(modulus: LargeInteger): LargeInteger;

      public toHexString(): string;

      public toByteArray(n?: number): Uint8Array;

      public bitLength(): number;

      public cmp(other: LargeInteger): number;

      public neg(): LargeInteger;

      public add(other: LargeInteger): LargeInteger;

      public sub(other: LargeInteger): LargeInteger;

      public mul(other: LargeInteger): LargeInteger;

      public square(): LargeInteger;

      public divQR(other: LargeInteger): LargeInteger[];

      public equals(other: LargeInteger): boolean;

      public legendre(prime: LargeInteger): number;

      public getBit(index: number): number;

      public iszero(): boolean;

      public shiftLeft(offset: number): LargeInteger;

      public shiftRight(offset: number): LargeInteger;
    }
  }

  namespace crypto {
    class RandomDevice {
    }
  }
}
